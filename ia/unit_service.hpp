#ifndef AI_UNIT_SERVICE_HPP
#define AI_UNIT_SERVICE_HPP

namespace ia
{
	class UnitService
	{
	public:
		static Unit* getUnitByID(string unitID, Player& player)
		{
			for (Unit unit : player.units)
			{
				if (unit.id == unitID)
					return &unit;
			}
			return nullptr;
		}
	};
}

#endif