namespace ia
{
	/// <summary>
	/// Store and update all the clusters on the map.
	/// The main should only have to use this class to interact with clusters.
	/// </summary>
	class ClusterManager
	{

		#pragma region Private
	private:

		GameMap& m_gameMap;
		Pathfinder& m_pathfinder;
		vector<Cluster*> m_clusters;

		const float EXPLORE_WHEN_UNITS_BY_CLUSTERS_SUPERIOR = 3;

		/// <summary>
		/// For all unit, check if they have a cluster or should change cluster and assign it.
		/// </summary>
		void updateUnitAssignations(Player& player)
		{
			for (auto& unit : player.units)
			{
				Cluster* cluster = getAssignedCluster(unit.id);
				if (cluster == nullptr)
				{
					vector<Cluster*> clusters = getBestClusters(unit.pos, ResourceServices::bestResourceType(player));
					if (clusters.size() == 0) return;

					bool exploreNewCluster = player.units.size() / clusters.size() >= EXPLORE_WHEN_UNITS_BY_CLUSTERS_SUPERIOR;

					int clusterIndex = 0;
					for (clusterIndex = 0; clusterIndex <= clusters.size(); clusterIndex++)
					{
						Cluster* c = clusters[clusterIndex];
						if (exploreNewCluster && c->getUnitCount() == 0 || // Cluster not explored so go
							!exploreNewCluster && c->getUnitCount() < c->getPerimeterSize(m_gameMap) / 2) // Cluster has not enough units so go
						{
							cluster = c;
							break;
						}
					}

					cluster->assignUnit(unit.id);
				}
			}
		}

		/// <summary>
		/// Get the cluster assigned to a unit.
		/// Return nullptr if no clusters assign to the unit given.
		/// </summary>
		Cluster* getAssignedCluster(string unitID)
		{
			for (Cluster* cluster : m_clusters)
			{
				if (cluster->hasUnitAssigned(unitID))
					return cluster;
			}
			return nullptr;
		}

		/// <summary>
		/// Gives list of harvestable clusters, ordered by their score.
		/// </summary>
		/// <param name="pos">The score of each clusters mainly depends on their distance from a specific position.</param>
		/// <param name="resourceType">The best resource type the player can harvest. If uranium, every clusters will be returned.</param>
		vector<Cluster*> getBestClusters(Position pos, ResourceType resourceType)
		{
			vector<Cluster*> sortedClusters = vector<Cluster*>(m_clusters);
			for (auto it = sortedClusters.begin(); it != sortedClusters.end(); it++)
			{
				Cluster* cluster = *it;
				if ((resourceType == ResourceType::wood && (cluster->getResourceType() == ResourceType::coal || cluster->getResourceType() == ResourceType::uranium)) ||
					(resourceType == ResourceType::coal && cluster->getResourceType() == ResourceType::uranium))
				{
					sortedClusters.erase(it--);
				}
			}

			sort(sortedClusters.begin(), sortedClusters.end(), [pos](Cluster* a, Cluster* b) { return a->getScore(pos) > b->getScore(pos); });
			return sortedClusters;
		}

		#pragma endregion

		#pragma region Public
	public:
		ClusterManager(GameMap& gameMap, Pathfinder& pathfinder) : m_gameMap(gameMap), m_pathfinder(pathfinder)
		{ }

		/// <summary>
		/// Create the clusters from the game map, separated by resource types.
		/// </summary>
		void initialize()
		{
			auto woodCells = ResourceServices::getResources(m_gameMap, ResourceType::wood);
			auto coalCells = ResourceServices::getResources(m_gameMap, ResourceType::coal);
			auto uraniumCells = ResourceServices::getResources(m_gameMap, ResourceType::uranium);

			auto woodGroups = MapAnalysis::getResourceGroups(woodCells);
			auto coalGroups = MapAnalysis::getResourceGroups(coalCells);
			auto uraniumGroups = MapAnalysis::getResourceGroups(uraniumCells);

			for (auto woodGroup : woodGroups)
			{
				m_clusters.push_back(new Cluster(woodGroup, ResourceType::wood));
			}

			for (auto coalGroup : coalGroups)
			{
				m_clusters.push_back(new Cluster(coalGroup, ResourceType::coal));
			}

			for (auto uraniumGroup : uraniumGroups)
			{
				m_clusters.push_back(new Cluster(uraniumGroup, ResourceType::uranium));
			}
		}

		/// <summary>
		/// This function should be called every turn. It updates clusters and missions before stacking units actions.
		/// </summary>
		/// <returns>List of actions that should be send at the end of the turn (mainly unit actions).</returns>
		vector<string> update(Player& player)
		{
			updateUnitAssignations(player);

			vector<string> actions;
			auto resourceCells = ResourceServices::getResources(m_gameMap);
			for (auto it = m_clusters.begin(); it != m_clusters.end(); it++)
			{
				Cluster* cluster = *it;

				bool notEmpty = cluster->update(resourceCells);
				if (!notEmpty)
				{
					m_clusters.erase(it--);
					continue;
				}

				cluster->updateMissions(m_gameMap, player);

				vector<string> clusterActions = cluster->doUnitActions(m_gameMap, m_pathfinder, player);
				actions.insert(actions.end(), clusterActions.begin(), clusterActions.end());
			}

			return actions;
		}

		/* --- GETTERS --- */
		int getClusterCount()
		{
			return m_clusters.size();
		}

		#pragma endregion

	};
}