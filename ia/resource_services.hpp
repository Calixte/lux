#ifndef AI_RESOURCE_SERVICES_HPP
#define AI_RESOURCE_SERVICES_HPP

namespace ia
{
	using namespace lux;

    /// <summary>
    /// Utils class for resource calculation.
    /// </summary>
    class ResourceServices
    {
    public:
        /// <summary>
        /// Get all resources of a specific type on the map.
        /// </summary>
        static vector<Cell*> getResources(GameMap& gameMap, ResourceType resourceType)
        {
            vector<Cell*> resourceCells = vector<Cell*>();
            for (int y = 0; y < gameMap.height; y++)
            {
                for (int x = 0; x < gameMap.width; x++)
                {
                    Cell* cell = gameMap.getCell(x, y);
                    if (cell->hasResource() && cell->resource.type == resourceType)
                    {
                        resourceCells.push_back(cell);
                    }
                }
            }
            return resourceCells;
        }

        /// <summary>
        /// Get all resources of the map.
        /// </summary>
        static vector<Cell*> getResources(GameMap& gameMap)
        {
            vector<Cell*> resourceCells = vector<Cell*>();
            for (int y = 0; y < gameMap.height; y++)
            {
                for (int x = 0; x < gameMap.width; x++)
                {
                    Cell* cell = gameMap.getCell(x, y);
                    if (cell->hasResource())
                    {
                        resourceCells.push_back(cell);
                    }
                }
            }
            return resourceCells;
        }

        /// <summary>
        /// Get all harvestable resources by the player on the map.
        /// </summary>
        static vector<Cell*> getHarvestableResources(GameMap& gameMap, Player& player)
        {
            return getHarvestableResources(getResources(gameMap), player);
        }

        /// <summary>
        /// Get all harvestable resources by the player in the cells given.
        /// </summary>
        static vector<Cell*> getHarvestableResources(vector<Cell*> resources, Player& player)
        {
            vector<Cell*> minableResources;
            for (auto resourceCell : resources)
            {
                switch (resourceCell->resource.type)
                {
                case ResourceType::wood:
                    minableResources.push_back(resourceCell);
                    break;
                case ResourceType::coal:
                    if (player.researchedCoal())
                    {
                        minableResources.push_back(resourceCell);
                    }
                    break;
                case ResourceType::uranium:
                    if (player.researchedUranium())
                    {
                        minableResources.push_back(resourceCell);
                    }
                    break;
                }
            }
        }

        /// <summary>
        /// Get resource type name.
        /// </summary>
        static string getResourceTypeName(ResourceType resourceType)
        {
            switch (resourceType)
            {
            case ResourceType::wood:
                return "WOOD";
                break;

            case ResourceType::coal:
                return "COAL";
                break;

            case ResourceType::uranium:
                return "URANIUM";
                break;

            default:
                return "";
                break;
            }
        }

        /// <summary>
        /// Get best resource type harvestable by the player : wood < coal < uranium.
        /// </summary>
        static ResourceType bestResourceType(Player& player)
        {
            if (player.researchedUranium()) return ResourceType::uranium;
            if (player.researchedCoal()) return ResourceType::uranium;
            return ResourceType::wood;
        }
    };
}

#endif