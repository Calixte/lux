#ifndef AI_CLUSTER_HPP
#define AI_CLUSTER_HPP

namespace ia
{
	/// <summary>
	/// A cluster is a group of adjacent resources of the same type.
	/// The strategy of this AI is based around clusters and securing them to avoid opponent from getting resources in the cluster.
	/// </summary>
	class Cluster
	{

		#pragma region Private
	private:
		vector<Cell*> m_cells;
		vector<string> m_unitIDs;
		vector<string> m_revokedUnitIDs; // Not used yet. Revoking causes 1 turn of cooldown for now. This is in prevention of fixing this.
		ResourceType m_resourceType;
		vector<Mission> m_missions;
		const float SCORE_FUEL_MULTIPLIER = 2;
		const float SCORE_DIST_MULTIPLIER = 1;
		const float SCORE_UNIT_MULTIPLIER = 4;
		const float BUILD_CITY_SCORE_NEIGHBOR_INFLUENCE = 1;

		/// <summary>
		/// Find first cell not already assign to a mission. This avoids to have two missions targetting the same cell.
		/// </summary>
		/// <param name="cells">List of possible cells for the mission. This should be sorted from the most to the least attractive since it returns the first possibly assignable cell.</param>
		Cell* findFirstCellNotAssignedToMission(vector<Cell*> cells)
		{
			int index = -1;
			bool posAssigned = true;
			do
			{
				posAssigned = false;
				index++;
				for (auto mission : m_missions)
				{
					if (mission.getTargetPos() == cells[index]->pos)
					{
						posAssigned = true;
						break;
					}
				}
			} while (posAssigned && index < cells.size());

			if (index >= cells.size())
				return nullptr;
			return cells[index];
		}

		/// <summary>
		/// Returns all the resources of the clusters sorted from the closest to the farest.
		/// </summary>
		vector<Cell*> getClosestResourceCells(Position pos)
		{
			vector<Cell*> closestCells = vector<Cell*>(m_cells);
			sort(closestCells.begin(), closestCells.end(), [pos](Cell* a, Cell* b) { return pos.distanceTo(a->pos) < pos.distanceTo(b->pos); });
			return closestCells;
		}

		/// <summary>
		/// Returns a list of all empty cells in the perimeter, sorted by the most to the least attractive from the position given.
		/// </summary>
		vector<Cell*> getOrderedFreePerimeterCells(GameMap& gameMap, Position pos)
		{
			struct ScoredCell
			{
				Cell* cell;
				float score;
			};

			vector<ScoredCell*> perimeterCells;
			for (auto cell : m_cells)
			{
				auto neighborsPos = MapAnalysis::getAdjacentNeighborsPos(cell->pos);
				for (auto neighborPos : neighborsPos)
				{
					if (neighborPos.x >= gameMap.width || neighborPos.x < 0 || neighborPos.y >= gameMap.height || neighborPos.y < 0)
						continue;

					auto neighbor = gameMap.getCellByPos(neighborPos);
					if (neighbor == nullptr || neighbor->hasResource() || neighbor->citytile != nullptr)
					{
						continue;
					}

					auto foundNeighbor = find_if(perimeterCells.begin(), perimeterCells.end(), [neighbor](ScoredCell* scoredCell) { return scoredCell->cell->pos == neighbor->pos; });
					if (foundNeighbor == perimeterCells.end()) // First time we check the cell
					{
						perimeterCells.push_back(new ScoredCell({ neighbor , -pos.distanceTo(neighborPos) }));
					}
					else // If we already checked it, we improve the score
					{
						(*foundNeighbor)->score += BUILD_CITY_SCORE_NEIGHBOR_INFLUENCE;
					}
				}
			}

			vector<Cell*> ordererCells;
			sort(perimeterCells.begin(), perimeterCells.end(), [](ScoredCell* a, ScoredCell* b) { return a->score > b->score; });
			for (auto scoredCell : perimeterCells)
			{
				ordererCells.push_back(scoredCell->cell);
			}

			return ordererCells;
		}

		/// <summary>
		/// Returns all the cells of the perimeter of the cell. The perimeter is all the cells around the cluster adjacent to the resources.
		/// If a player manages to build citytiles on all the perimeter cells of a cluster, the opponent cannot access the resources of that cluster.
		/// The strategy of this AI is based on securing the clusters to have permanent gain of resources.
		/// </summary>
		vector<Cell*> getPerimeter(GameMap& gameMap)
		{
			vector<Cell*> perimeterCells;
			for (auto cell : m_cells)
			{
				auto neighborsPos = MapAnalysis::getAdjacentNeighborsPos(cell->pos);
				for (auto neighborPos : neighborsPos)
				{
					if (neighborPos.x >= gameMap.width || neighborPos.x < 0 || neighborPos.y >= gameMap.height || neighborPos.y < 0)
						continue;

					auto neighbor = gameMap.getCellByPos(neighborPos);
					if (neighbor == nullptr || neighbor->hasResource())
						continue;

					auto foundNeighbor = find_if(perimeterCells.begin(), perimeterCells.end(), [neighbor](Cell* cell) { return cell->pos == neighbor->pos; });
					if (foundNeighbor == perimeterCells.end()) // First time we check the cell
					{
						perimeterCells.push_back(neighbor);
					}
				}
			}
			return perimeterCells;
		}

		/// <summary>
		/// Resources count of a clusters, converted into fuel. Used to calcul score.
		/// </summary>
		int getAvailableFuel()
		{
			int availableFuel = 0;
			for (auto cell : m_cells)
			{
				int fuelConvertionRate = GAME_CONSTANTS["PARAMETERS"]["RESOURCE_TO_FUEL_RATE"]
					[ResourceServices::getResourceTypeName(m_resourceType)];
				availableFuel += cell->resource.amount * fuelConvertionRate;
			}
			return availableFuel;
		}

		/// <summary>
		/// Get a score of resources for the cluster. It is available fuel / n� of resource tiles.
		/// </summary>
		float getFuelDensity()
		{
			return getAvailableFuel() / m_cells.size();
		}

		/// <summary>
		/// The centroid simplifies the position of the cluster. Used to calcul positional score of the cluster.
		/// </summary>
		Position getCentroid()
		{
			if (m_cells.size() == 0)
				return Position(-1, -1);

			Position sum = Position(0, 0);
			for (auto cell : m_cells)
			{
				sum.x += cell->pos.x;
				sum.y += cell->pos.y;
			}
			return Position(sum.x / m_cells.size(), sum.y / m_cells.size());
		}

		#pragma endregion

		#pragma region Public
	public:
		Cluster(vector<Cell*> cellGroup, ResourceType resourceType) : m_cells(cellGroup), m_resourceType(resourceType)
		{ }

		/// <summary>
		/// Update the cluster resources.
		/// </summary>
		/// <returns>Returns false when there is no resource tiles left and the cluster needs to be removed from the list.</returns>
		bool update(vector<Cell*> resourceCells)
		{
			for (auto it = m_cells.begin(); it != m_cells.end(); it++)
			{
				if (find(resourceCells.begin(), resourceCells.end(), *it) != resourceCells.end()) // !contains
				{
					m_cells.erase(it--);
				}
			}

			return m_cells.size() > 0;
		}

		/// <summary>
		/// Update cluster missions and assign them to units.
		/// Priority is securing the cluster by building cities around. Then guard the cities.
		/// </summary>
		void updateMissions(GameMap& gameMap, Player& player)
		{
			// Remove completed missions
			for (auto it = m_missions.begin(); it != m_missions.end(); it++)
			{
				Mission mission = *it;
				if (mission.isComplete())
				{
					m_missions.erase(it--);
					continue;
				}
			}

			for (auto it = m_unitIDs.begin(); it != m_unitIDs.end(); it++)
			{
				string unitID = *it;

				// Get unit from id
				Unit* unit = nullptr;
				for (Unit u : player.units)
				{
					if (u.id == unitID)
					{
						unit = &u;
						break;
					}
				}
				if (unit == nullptr)
				{ 
					m_unitIDs.erase(it--);
					continue;
				}

				// Cooldown check
				if (!unit->canAct())
					continue;

				auto perimeter = getOrderedFreePerimeterCells(gameMap, unit->pos);

				// Check if units mission are still valid, based on the map update and unit resources
				bool hasValidMission = false;
				for (auto it = m_missions.begin(); it != m_missions.end(); it++)
				{
					Mission mission = *it;
					if (mission.getUnitID() == unitID)
					{
						hasValidMission = mission.isMissionStillValid(gameMap, perimeter, unit);
						if (!hasValidMission)
						{
							// Can't do his mission anymore, we delete it.
							m_missions.erase(it--);
						}

						break;
					}
				}
				if (hasValidMission)
				{
					// The unit can continue his mission
					continue;
				}

				auto missionType = Mission::chooseMissionType(perimeter, unit);
				switch (missionType)
				{
					case BUILD_CITY:
					{
						Cell* bestPerimeterCell = findFirstCellNotAssignedToMission(perimeter);
						if (bestPerimeterCell == nullptr)
						{
							// Change cluster since perimeter is already full
							m_unitIDs.erase(it--);
							m_revokedUnitIDs.push_back(unit->id);
						}
						else
						{
							m_missions.push_back(Mission(unitID, bestPerimeterCell->pos, BUILD_CITY));
						}
						break;
					}
					case COLLECT_RESOURCE:
					{
						vector<Cell*> resourceCells = getClosestResourceCells(unit->pos);
						Cell* bestResourceCell = findFirstCellNotAssignedToMission(resourceCells);
						if (bestResourceCell == nullptr)
						{
							// Change cluster since there is already more units than resource cells in this cluster
							m_unitIDs.erase(it--);
							m_revokedUnitIDs.push_back(unit->id);
						}
						else
						{
							m_missions.push_back(Mission(unitID, bestResourceCell->pos, COLLECT_RESOURCE));
						}
						break;
					}
					case GUARD_CLUSTER:
						// TODO : Should get resources and stay on city tiles to make them survive. This is a passive mission that can't be completed.
						// TODO : If a city tile is destroyed, the unit should automatically change mission to build a new city to resecure the cluster.
						// TODO : This haven't be implemented yet since not everything works. We previewed to do this at the end but we didn't have enough time.
						// TODO : However this mission type is unavoidable to make the strategy work. Otherwise the clusters could die easily.
						break;

					default:
						break;
				}
			}
		}

		/// <summary>
		/// Call unit actions, based on their assignated missions.
		/// </summary>
		/// <returns>List of actions that should be send at the end of the turn.</returns>
		vector<string> doUnitActions(GameMap& gameMap, Pathfinder& pathfinder, Player& player)
		{
			vector<string> actions;
			for (auto it = m_missions.begin(); it != m_missions.end(); it++)
			{
				Mission mission = *it;

				// Get unit assignated to the mission
				Unit* unit = nullptr;
				for (Unit u : player.units)
				{
					if (u.id == mission.getUnitID())
					{
						unit = &u;
						break;
					}
				}

				// Units can be destroyed, we need to erase the mission if the unit is not found
				if (unit == nullptr)
				{
					m_missions.erase(it--);
					continue;
				}

				if (!unit->canAct())
					continue;

				// Debug : shows where the unit wants to go
				actions.push_back(Annotate::circle(mission.getTargetPos().x, mission.getTargetPos().y));
				actions.push_back(Annotate::line(unit->pos.x, unit->pos.y, mission.getTargetPos().x, mission.getTargetPos().y));

				switch (mission.getType())
				{
					case BUILD_CITY:
					{
						// Debug : X for city build missions, O for resource collect
						actions.push_back(Annotate::x(mission.getTargetPos().x, mission.getTargetPos().y));
						if (unit->pos == mission.getTargetPos())
						{
							if (unit->canBuild(gameMap))
							{
								actions.push_back(unit->buildCity());
								mission.setComplete(); // The mission will be deleted next turn and a new mission will be assignated to the unit
							}
						}
						else
						{
							actions.push_back(pathfinder.moveUnit(unit, mission.getTargetPos()));
						}
						break;
					}
					case COLLECT_RESOURCE:
					{
						// Wait if already on the mission target position. Mission will automatically change when checking if the mission is still valid.
						if (unit->pos != mission.getTargetPos())
						{
							actions.push_back(pathfinder.moveUnit(unit, mission.getTargetPos()));
						}
						break;
					}

					case GUARD_CLUSTER:
						// TODO : see update missions method
						break;

					default:
						break;
				}
			}
			return actions;
		}

		#pragma region Utils and Calculations

		/// <summary>
		/// Returns true if unit ID is assignated to this cluster.
		/// </summary>
		bool hasUnitAssigned(string unitID)
		{
			return find(m_unitIDs.begin(), m_unitIDs.end(), unitID) != m_unitIDs.end();
		}

		/// <summary>
		/// Assign the unit ID to this cluster. Missions will be assignated to this unit.
		/// </summary>
		void assignUnit(string unitID)
		{
			m_unitIDs.push_back(unitID);
		}

		/// <summary>
		/// Get score based on distance and resources
		/// </summary>
		float getScore(Position pos)
		{
			float distScore = -pos.distanceTo(getCentroid()) * SCORE_DIST_MULTIPLIER;
			float fuelScore = getFuelDensity() * SCORE_FUEL_MULTIPLIER;
			float unitScore = -m_unitIDs.size() * SCORE_UNIT_MULTIPLIER;
			return fuelScore + distScore + unitScore;
		}

		/// <summary>
		/// Returns the number of cells of the perimeter
		/// </summary>
		int getPerimeterSize(GameMap& gameMap)
		{
			return getPerimeter(gameMap).size();
		}

		#pragma endregion

		#pragma region Getters

		int getUnitCount()
		{
			return m_unitIDs.size();
		}

		ResourceType getResourceType()
		{
			return m_resourceType;
		}

		#pragma endregion

		#pragma endregion

	};
}

#endif