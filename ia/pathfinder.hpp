#ifndef IA_PATHFINDER_HPP
#define IA_PATHFINDER_HPP

namespace ia
{
    using namespace lux;

    /// <summary>
    /// Used to make smart pathfind and unit movements.
    /// This uses a graph to avoid certain cells by selecting sorted best directions.
    /// This avoids collisions and other unwanted behaviors.
    /// This is major for making this AI work.
    /// We tried using Dijkstra but couldn't make it work.
    /// We tried another solution but couldn't make it work either.
    /// </summary>
    class Pathfinder
    {
    private:
        enum ETile { WALKABLE = 0 , OBSTACLE = 9 };
        vector<vector<ETile>> m_graph;
        const int MAX_INT = 99999;

        /// <summary>
        /// Dijkstra's single source shortest path algorithm.
        /// </summary>
        /// <returns>Every move to get to the target from the source using the shortest path.</returns>
        vector<Position> dijkstra(Position source, Position target)
        {
            if (source.x == target.x && source.y == target.y)
            {
                cout << "source == target" << endl;
                return vector<Position>({ source });
            }
            if (m_graph[target.y][target.x] == OBSTACLE)
            {
                cout << "target is a 9" << endl;
                return vector<Position>({ source });
            }
            // if all the cells around source are blocked
            if (m_graph[source.y - 1][source.x] == OBSTACLE && m_graph[source.y + 1][source.x] == OBSTACLE && m_graph[source.y][source.x - 1] == OBSTACLE && m_graph[source.y][source.x + 1] == OBSTACLE)
            {
                cout << "source is surrounded by 9" << endl;
                return vector<Position>({ source });
            }
            // same for target
            if (m_graph[target.y - 1][target.x] == 9 && m_graph[target.y + 1][target.x] == 9 && m_graph[target.y][target.x - 1] == 9 && m_graph[target.y][target.x + 1] == 9)
            {
                cout << "target is surrounded by 9" << endl;
                return vector<Position>({ source });
            }

            // Initialize variables and data structures
            int H = m_graph.size();
            int W = m_graph[0].size();
            vector<vector<int>> dist = vector<vector<int>>(H, vector<int>(W, MAX_INT));
            vector<vector<bool>> sptSet = vector<vector<bool>>(H, vector<bool>(W, false));
            vector<vector<Position>> parent = vector<vector<Position>>(H, vector<Position>(W, { -1, -1 }));

            dist[source.y][source.x] = WALKABLE;
            for (int count = 0; count < H * W - 1; count++)
            {
                // Find the minimum distance vertex from the set of vertices not yet processed
                int min_dist = MAX_INT;
                int min_x = -1;
                int min_y = -1;
                for (int i = 0; i < H; i++)
                {
                    for (int j = 0; j < W; j++)
                    {
                        if (!sptSet[i][j] && dist[i][j] <= min_dist)
                        {
                            min_dist = dist[i][j];
                            min_x = j;
                            min_y = i;
                        }
                    }
                }

                // If the target vertex has been processed, exit the loop
                if (sptSet[target.y][target.x])
                {
                    break;
                }

                // Mark the picked vertex as processed
                sptSet[min_y][min_x] = true;

                // Update the distances of the adjacent vertices of the picked vertex
                for (int i = -1; i <= 1; i++)
                {
                    for (int j = -1; j <= 1; j++)
                    {
                        if (i == 0 && j == 0)
                        {
                            continue;
                        }
                        if (abs(i) + abs(j) == 2)
                        {
                            continue;
                        }
                        int new_x = min_x + j;
                        int new_y = min_y + i;
                        if (new_x >= 0 && new_x < W && new_y >= 0 && new_y < H && m_graph[new_y][new_x] != OBSTACLE)
                        {
                            int adjacent_dist = abs(i) + abs(j);
                            if (!sptSet[new_y][new_x] && dist[min_y][min_x] + adjacent_dist < dist[new_y][new_x])
                            {
                                dist[new_y][new_x] = dist[min_y][min_x] + adjacent_dist;
                                parent[new_y][new_x] = { min_x, min_y };
                            }
                        }
                    }
                }
            }

            // If the target vertex is unreachable, return an empty vector
            if (dist[target.y][target.x] == MAX_INT)
            {
                return vector<Position>({ source });
            }

            // construct the shortest path
            vector<Position> path;
            int current_x = target.x;
            int current_y = target.y;

            while (current_x != source.x || current_y != source.y)
            {
                path.push_back({ current_x, current_y });
                int temp_x = parent[current_y][current_x].x;
                int temp_y = parent[current_y][current_x].y;
                current_x = temp_x;
                current_y = temp_y;
            }

            reverse(path.begin(), path.end());

            // print the path
            /*for (const auto & pos : path)
            {
                cout << pos.x << ":" << pos.y << endl;
            }
            cout << "end dijkstra" << endl;*/
            return path;
        }

        /// <summary>
        /// Improved directionTo() method of the Position class.
        /// This method should work but when using it, it times out.
        /// </summary>
        DIRECTIONS calculateNextDirection(Position currentPosition, Position targetPosition, bool changeToObstacle = false)
        {
            vector<DIRECTIONS> directions = vector<DIRECTIONS>({ NORTH , SOUTH , EAST , WEST , CENTER });
            sort(directions.begin(), directions.end(), [currentPosition, targetPosition](DIRECTIONS a, DIRECTIONS b) {
                return currentPosition.translate(a, 1).distanceTo(targetPosition) < currentPosition.translate(b, 1).distanceTo(targetPosition);
            });

            Position nextPosition = currentPosition;
            int chosenIndex = 0;
            while (chosenIndex < directions.size())
            {
                Position temp = currentPosition.translate(directions[chosenIndex], 1);
                if (temp.y < 0 || temp.y >= m_graph.size() || temp.x < 0 || temp.x >= m_graph[0].size())
                {
                    chosenIndex++;
                    continue;
                }
                nextPosition = temp;
                if (m_graph[nextPosition.y][nextPosition.x] == WALKABLE)
                {
                    break;
                }
                chosenIndex++;
            }

            /*if (changeToObstacle)
            {
                Position nextPosition = currentPosition.translate(directions[chosenIndex], 1);
                m_graph[nextPosition.y][nextPosition.x] = OBSTACLE;
            }*/

            return directions[chosenIndex];
        }
    public:

        /// <summary>
        /// Update the graph with the current game map (walkable and obstacles).
        /// </summary>
        void updateGraph(GameMap& gameMap, vector<Unit>& units)
        {
            m_graph = vector<vector<ETile>>(gameMap.width, vector<ETile>(gameMap.height, WALKABLE));
            for (int i = 0; i < gameMap.width; i++)
            {
                for (int j = 0; j < gameMap.height; j++)
                {
                    // Add city tiles as obstacle to avoid colliding with opponent's city tiles and dropping resources on city if we don't want to
                    if (gameMap.map[i][j].citytile != nullptr)
                    {
                        m_graph[i][j] = OBSTACLE;
                    }
                }
            }

            // Add units that can't move to avoid collisions
            for (Unit unit : units)
            {
                if (unit.canAct())
                    continue;
                m_graph[unit.pos.y][unit.pos.x] = OBSTACLE;
            }
        }

        /// <summary>
        /// Make the action of moving a unit to a target pos using the pathfinding.
        /// </summary>
        /// <returns>The action of the unit moving. Should be send at the end of the turn.</returns>
        string moveUnit(Unit* unit, Position targetPos)
        {
            // Dijkstra
            /*auto moves = dijkstra(unit->pos, targetPos);
            Position nextMove = moves[moves.size()-1];
            m_graph[nextMove.y][nextMove.x] = OBSTACLE;
            return unit->move(unit->pos.directionTo(nextMove));*/

            // Improved directionTo
            /*DIRECTIONS nextDirection = calculateNextDirection(unit->pos, targetPos, true);
            return unit->move(nextDirection);*/

            // Classical directionTo
            return unit->move(unit->pos.directionTo(targetPos));
        }
    };
}

#endif