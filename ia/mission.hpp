#ifndef AI_MISSION_HPP
#define AI_MISSION_HPP

namespace ia
{
	/// <summary>
	/// COLLECT_RESOURCE : Get enough resources to be full
	/// BUILD_CITY : Build a city to the best cell of the cluster
	/// GUARD_CLUSTER : Keep the city tiles around the cluster alive
	/// </summary>
	enum EMissionType
	{ COLLECT_RESOURCE , BUILD_CITY , GUARD_CLUSTER };

	/// <summary>
	/// Missions are assignated on units by clusters. The choice, validity and action of the mission defines the strategy of the AI.
	/// </summary>
	class Mission
	{
	private:
		EMissionType m_type;
		Position m_targetPos;
		string m_unitID;
		bool m_complete = false;

	public:
		Mission(string unitID, Position targetPos, EMissionType type) : m_unitID(unitID), m_targetPos(targetPos), m_type(type)
		{ }

		/// <summary>
		/// Choose the mission, based on the cluster perimeter current state and the unit resources.
		/// </summary>
		static EMissionType chooseMissionType(vector<Cell*>& perimeter, Unit* unit)
		{
			if (perimeter.size() == 0)
			{
				return GUARD_CLUSTER;
			}
			else if (unit->getCargoSpaceLeft() == 0)
			{
				return BUILD_CITY;
			}
			else
			{
				return COLLECT_RESOURCE;
			}
		}

		/// <summary>
		/// Check if the unit mission is still valid or if it should change.
		/// </summary>
		bool isMissionStillValid(GameMap& gameMap, vector<Cell*>& perimeter, Unit* unit)
		{
			if (chooseMissionType(perimeter, unit) != m_type)
				return false;

			Cell* targetCell = gameMap.getCell(m_targetPos.x, m_targetPos.y);
			switch (m_type)
			{
			case BUILD_CITY:
				return targetCell->citytile == nullptr && !targetCell->hasResource();
				break;

			case COLLECT_RESOURCE:
				return targetCell->hasResource();
				break;

			default:
				return true;
				break;
			}
		}

		#pragma region Getters Setters

		EMissionType getType()
		{
			return m_type;
		}
		Position getTargetPos()
		{
			return m_targetPos;
		}
		string getUnitID()
		{
			return m_unitID;
		}
		bool isComplete()
		{
			return m_complete;
		}
		void setComplete()
		{
			m_complete = true;
		}

		#pragma endregion
	};
}

#endif