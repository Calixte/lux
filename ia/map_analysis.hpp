#ifndef AI_MAP_ANALYSIS_HPP
#define AI_MAP_ANALYSIS_HPP

namespace ia
{
    using namespace lux;

    /// <summary>
    /// Utils class.
    /// Used to analyse the game map and make methods easier.
    /// </summary>
    class MapAnalysis
    {
    private:
        struct CellNode
        {
            Position pos;
            bool visited;
            Cell tile;
        };

        /// <summary>
        /// Depth first search algorithm.
        /// Used to find all connected nodes to a given node in a graph.
        /// </summary>
        static vector<CellNode*> dfs(vector<CellNode*>& nodes, CellNode* node)
        {
            vector<CellNode*> group;

            vector<CellNode*> st;
            st.push_back(node);

            while (!st.empty())
            {
                node = st.back();
                st.pop_back();
                group.push_back(node);

                vector<CellNode*> neighbors = findNeighbors(node, nodes);

                for (CellNode* neighbor : neighbors)
                {
                    if (!neighbor->visited)
                    {
                        st.push_back(neighbor);
                        neighbor->visited = true;
                    }
                }
            }

            return group;
        }

        /// <summary>
        /// Find the 8 neighbors of a cellnode (diags included)
        /// </summary>
        static vector<CellNode*> findNeighbors(CellNode* cellToCheck, vector<CellNode*>& allCellNodes)
        {
            vector<Position> neighborPositions = getNeighborsPos(cellToCheck->pos);
            vector<CellNode*> neighbors;
            for (CellNode* tile : allCellNodes)
            {
                if (std::find(neighborPositions.begin(), neighborPositions.end(), tile->pos) != neighborPositions.end())
                {
                    neighbors.push_back(tile);
                }
            }
            return neighbors;
        }

        /// <summary>
        /// Find the 8 neighbors of a cell (diags included)
        /// </summary>
        static vector<Position> getNeighborsPos(Position pos)
        {
            vector<Position> neighborPositions;
            auto north = pos.translate(DIRECTIONS::NORTH, 1);
            auto south = pos.translate(DIRECTIONS::SOUTH, 1);
            neighborPositions.push_back(north);
            neighborPositions.push_back(south);
            neighborPositions.push_back(pos.translate(DIRECTIONS::EAST, 1));
            neighborPositions.push_back(pos.translate(DIRECTIONS::WEST, 1));
            neighborPositions.push_back(north.translate(DIRECTIONS::EAST, 1));
            neighborPositions.push_back(north.translate(DIRECTIONS::WEST, 1));
            neighborPositions.push_back(south.translate(DIRECTIONS::EAST, 1));
            neighborPositions.push_back(south.translate(DIRECTIONS::WEST, 1));
            return neighborPositions;
        }

    public:

        /// <summary>
        /// Find the 4 neighbors of a cell (diags not included)
        /// </summary>
        static vector<Position> getAdjacentNeighborsPos(Position pos)
        {
            vector<Position> neighborPositions;
            neighborPositions.push_back(pos.translate(DIRECTIONS::NORTH, 1));
            neighborPositions.push_back(pos.translate(DIRECTIONS::SOUTH, 1));
            neighborPositions.push_back(pos.translate(DIRECTIONS::EAST, 1));
            neighborPositions.push_back(pos.translate(DIRECTIONS::WEST, 1));
            return neighborPositions;
        }

        /// <summary>
        /// Regroup adjacent ressources to groups.
        /// Used to find the clusters.
        /// </summary>
        static vector<vector<Cell *>> getResourceGroups(vector<Cell *> resource_cells)
        {
            vector<CellNode*> nodes;
            for (Cell *resource_cell : resource_cells)
            {
                nodes.push_back(new CellNode{resource_cell->pos, false, *resource_cell});
            }

            vector<vector<Cell *>> groups;
            for (CellNode* node : nodes)
            {
                if (!node->visited)
                {
                    vector<CellNode*> group = dfs(nodes, node);
                    vector<Cell *> cell_group;
                    for (CellNode* n : group)
                    {
                        cell_group.push_back(&n->tile);
                    }
                    groups.push_back(cell_group);
                }
            }

            return groups;
        }
    };
}

#endif