#include "lux/kit.hpp"
#include "lux/define.cpp"
#include "ia/kit.hpp"
#include <string.h>
#include <vector>

using namespace std;
using namespace lux;
using namespace ia;

int main()
{
    kit::Agent gameState = kit::Agent();
    // initialize
    gameState.initialize();

    GameMap &gameMap = gameState.map;
    Pathfinder pathfinder;
    ClusterManager clusterManager = ClusterManager(gameMap, pathfinder);

    while (true)
    {
        /** Do not edit! **/
        // wait for updates
        gameState.update();

        vector<string> actions = vector<string>();

        /** AI Code Goes Below! **/
        if (gameState.turn == 0)
        {
            clusterManager.initialize();
        }

        Player &player = gameState.players[gameState.id];
        Player &opponent = gameState.players[(gameState.id + 1) % 2];

        /* --- PATHFINDER UPDATE --- */
        pathfinder.updateGraph(gameMap, player.units);

		/* --- CLUSTERS & UNITS --- */
		vector<string> clusterActions = clusterManager.update(player);
		actions.insert(actions.end(), clusterActions.begin(), clusterActions.end());

		/* --- CITY --- */
        int buildWorkerActions = 0;
		for (auto& city : player.cities)
		{
			for (auto& cityTile : city.second.citytiles)
			{
				if (!cityTile.canAct())
					continue;
                // Priorities building workers
				if (player.units.size() + buildWorkerActions < player.cities.size())
				{
                    buildWorkerActions++;
					actions.push_back(cityTile.buildWorker());
				}
				else
				{
                    // If we can't we just farm research points
					actions.push_back(cityTile.research());
				}
			}
		}

    /** Do not edit! **/
    for (int i = 0; i < actions.size(); i++)
    {
      if (i != 0)
        cout << ",";
      cout << actions[i];
    }
    cout << endl;
    // end turn
    gameState.end_turn();
  }

  return 0;
}