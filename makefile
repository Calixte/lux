all : run

run:
	g++ main.cpp -O3 -std=c++11 -o main.out
	lux-ai-2021 main.out main.out --height 12 --width 12 --out=replay.json --seed 5000

random:
	g++ main.cpp -O3 -std=c++11 -o main.out
	lux-ai-2021 main.out main.out --out=replay.json

contest-1:
	g++ main.cpp -O3 -std=c++11 -o main.out
	lux-ai-2021 main.out main_Meriem_Johan.out --out=replay.json
	
contest-2:
	g++ main.cpp -O3 -std=c++11 -o main.out
	lux-ai-2021 main.out main_alexis_anouk_tom.out --out=replay.json
	
contest-3:
	g++ main.cpp -O3 -std=c++11 -o main.out
	lux-ai-2021 main.out main_Lucas_Axelle_Victor.out --out=replay.json

contest-4:
	g++ main.cpp -O3 -std=c++11 -o main.out
	lux-ai-2021 main.out main_jianqiao.out --out=replay.json

contest-5:
	g++ main.cpp -O3 -std=c++11 -o main.out
	lux-ai-2021 main.out main_MEL.out --out=replay.json

help:
	echo "go to https://2021vis.lux-ai.org/ to watch replays"
	lux-ai-2021 --help